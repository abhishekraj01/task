package com.glogistics.task.web;
import com.glogistics.task.CoreConfig;
import com.glogistics.task.exception.AppException;
import com.glogistics.task.persistence.Employee;
import com.glogistics.task.repository.EmployeeRepo;
import com.glogistics.task.service.serviceImpl.EmployeeServiceImpl;
import com.glogistics.task.web.beans.EmployeeBeen;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Abhishe Raj on 24/6/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {CoreConfig.class})
@Transactional
public class EmployeeControllerTest extends BaseControllerTest{

    @Autowired
    private MockMvc mockMvc;

    private Gson gson = new Gson();

    @Autowired
    private EmployeeRepo employeeRepo;

    @Test
    public void saveEmployeeTest() throws Exception {
        EmployeeBeen employeeBeenOne =  new EmployeeBeen();
        employeeBeenOne.setDesignation("Developer");
        employeeBeenOne.setName("Abhishek");
        employeeBeenOne.setDepartment("Software Development");
        employeeBeenOne.setGender("Male");
        employeeBeenOne.setSallery(98706.09);
        employeeBeenOne.setMobileNo(7838993975L);
        employeeBeenOne.setEmailId("abhishek@gmail.com");

        String employeeJson =  gson.toJson(employeeBeenOne);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        EmployeeBeen employeeBeenTwo = new EmployeeBeen();
        employeeBeenTwo.setName("Arun Kumar");
        employeeBeenTwo.setDepartment("Electrical Engg");
        employeeBeenTwo.setDesignation("Engg");
        employeeBeenTwo.setGender("Male");
        employeeBeenTwo.setSallery(9056.09);
        employeeBeenTwo.setMobileNo(9410637053L);
        employeeBeenTwo.setEmailId("arun@gmail.com");

        employeeJson =  gson.toJson(employeeBeenTwo);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

    }

    @Test
    public void getAllEmployeeTest() throws Exception{

        EmployeeBeen employeeBeenOne =  new EmployeeBeen();
        employeeBeenOne.setDesignation("Developer");
        employeeBeenOne.setName("Abhishek");
        employeeBeenOne.setDepartment("Software Development");
        employeeBeenOne.setGender("Male");
        employeeBeenOne.setSallery(98706.09);
        employeeBeenOne.setMobileNo(7838993975L);
        employeeBeenOne.setEmailId("abhishek@gmail.com");

        String employeeJson =  gson.toJson(employeeBeenOne);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        EmployeeBeen employeeBeenTwo = new EmployeeBeen();
        employeeBeenTwo.setName("Arun Kumar");
        employeeBeenTwo.setDepartment("Electrical Engg");
        employeeBeenTwo.setDesignation("Engg");
        employeeBeenTwo.setGender("Male");
        employeeBeenTwo.setSallery(9056.09);
        employeeBeenTwo.setMobileNo(9410637053L);
        employeeBeenTwo.setEmailId("arun@gmail.com");

        employeeJson =  gson.toJson(employeeBeenTwo);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        String url = "/employee";

        this.mockMvc.perform(get(url)).andDo(print()).andExpect(status().isOk());

    }

    @Test
    public void getEmployeeByIdTest() throws Exception{
        EmployeeBeen employeeBeenOne =  new EmployeeBeen();
        employeeBeenOne.setDesignation("Developer");
        employeeBeenOne.setName("Abhishek");
        employeeBeenOne.setDepartment("Software Development");
        employeeBeenOne.setGender("Male");
        employeeBeenOne.setSallery(98706.09);
        employeeBeenOne.setMobileNo(7838993975L);
        employeeBeenOne.setEmailId("abhishek@gmail.com");

        String employeeJson =  gson.toJson(employeeBeenOne);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        EmployeeBeen employeeBeenTwo = new EmployeeBeen();
        employeeBeenTwo.setName("Arun Kumar");
        employeeBeenTwo.setDepartment("Electrical Engg");
        employeeBeenTwo.setDesignation("Engg");
        employeeBeenTwo.setGender("Male");
        employeeBeenTwo.setSallery(9056.09);
        employeeBeenTwo.setMobileNo(9410637053L);
        employeeBeenTwo.setEmailId("arun@gmail.com");

        employeeJson =  gson.toJson(employeeBeenTwo);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        Employee employee = employees.get(0);
        String url = "/employee/"+employee.getEmpId();

        this.mockMvc.perform(get(url)).andDo(print()).andExpect(status().isOk());

    }


    @Test
    public void totalEmployeeTest()throws Exception{
        EmployeeBeen employeeBeenOne =  new EmployeeBeen();
        employeeBeenOne.setDesignation("Developer");
        employeeBeenOne.setName("Abhishek");
        employeeBeenOne.setDepartment("Software Development");
        employeeBeenOne.setGender("Male");
        employeeBeenOne.setSallery(98706.09);
        employeeBeenOne.setMobileNo(7838993975L);
        employeeBeenOne.setEmailId("abhishek@gmail.com");

        String employeeJson =  gson.toJson(employeeBeenOne);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        EmployeeBeen employeeBeenTwo = new EmployeeBeen();
        employeeBeenTwo.setName("Arun Kumar");
        employeeBeenTwo.setDepartment("Electrical Engg");
        employeeBeenTwo.setDesignation("Engg");
        employeeBeenTwo.setGender("Male");
        employeeBeenTwo.setSallery(9056.09);
        employeeBeenTwo.setMobileNo(9410637053L);
        employeeBeenTwo.setEmailId("arun@gmail.com");

        employeeJson =  gson.toJson(employeeBeenTwo);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        String url = "/all/employee";

        this.mockMvc.perform(get(url)).andDo(print()).andExpect(status().isOk());

    }


    @Test
    public void deleteEmployeeTest()throws Exception {
        EmployeeBeen employeeBeenOne =  new EmployeeBeen();
        employeeBeenOne.setDesignation("Developer");
        employeeBeenOne.setName("Abhishek");
        employeeBeenOne.setDepartment("Software Development");
        employeeBeenOne.setGender("Male");
        employeeBeenOne.setSallery(98706.09);
        employeeBeenOne.setMobileNo(7838993975L);
        employeeBeenOne.setEmailId("abhishek@gmail.com");

        String employeeJson =  gson.toJson(employeeBeenOne);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        EmployeeBeen employeeBeenTwo = new EmployeeBeen();
        employeeBeenTwo.setName("Arun Kumar");
        employeeBeenTwo.setDepartment("Electrical Engg");
        employeeBeenTwo.setDesignation("Engg");
        employeeBeenTwo.setGender("Male");
        employeeBeenTwo.setSallery(9056.09);
        employeeBeenTwo.setMobileNo(9410637053L);
        employeeBeenTwo.setEmailId("arun@gmail.com");

        employeeJson =  gson.toJson(employeeBeenTwo);

        this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        Employee employee = employees.get(0);
        String url = "/employee/"+employee.getEmpId();

        this.mockMvc.perform(delete(url)).andDo(print()).andExpect(status().isOk());

        employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

   }

   @Test
    public void updateEmployeeTest()throws Exception{
       EmployeeBeen employeeBeenOne =  new EmployeeBeen();
       employeeBeenOne.setDesignation("Developer");
       employeeBeenOne.setName("Abhishek");
       employeeBeenOne.setDepartment("Software Development");
       employeeBeenOne.setGender("Male");
       employeeBeenOne.setSallery(98706.09);
       employeeBeenOne.setMobileNo(7838993975L);
       employeeBeenOne.setEmailId("abhishek@gmail.com");

       String employeeJson =  gson.toJson(employeeBeenOne);

       this.mockMvc.perform(post("/employee").content(employeeJson).contentType(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk());

       List<Employee> employees = employeeRepo.findAll();
       Assert.assertEquals(1,employees.size());


       Employee employee = employees.get(0);
       String url = "/employee/"+employee.getEmpId();

       Employee oneEmployee = employeeRepo.findOne(employee.getEmpId());
       Assert.assertEquals("Abhishek", oneEmployee.getName());
       EmployeeBeen updateEmp =  new EmployeeBeen();
       updateEmp.setDesignation("Developer");
       updateEmp.setName("Abhishek Raj");
       updateEmp.setDepartment("Software Development");
       updateEmp.setGender("Male");
       updateEmp.setSallery(98706.09);
       updateEmp.setMobileNo(7838993975L);
       updateEmp.setEmailId("abhishek@gmail.com");

       employeeJson =  gson.toJson(updateEmp);

       this.mockMvc.perform(put(url).content(employeeJson).contentType(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk());

       oneEmployee = employeeRepo.findOne(employee.getEmpId());
       Assert.assertEquals("Abhishek Raj", oneEmployee.getName());



   }






}
