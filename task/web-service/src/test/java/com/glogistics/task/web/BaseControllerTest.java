package com.glogistics.task.web;

import com.glogistics.task.CoreConfig;
import com.glogistics.task.exception.AppException;
import com.glogistics.task.persistence.Employee;
import com.glogistics.task.repository.EmployeeRepo;
import com.glogistics.task.service.serviceImpl.EmployeeServiceImpl;
import com.google.gson.Gson;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Abhishek Raj on 24/6/17.
 */


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {CoreConfig.class})
@Transactional
public abstract class BaseControllerTest {




}
