package com.glogistics.task.web.controller;

import com.glogistics.task.exception.AppException;
import com.glogistics.task.persistence.Employee;
import com.glogistics.task.repository.EmployeeRepo;
import com.glogistics.task.service.serviceImpl.EmployeeServiceImpl;
import com.glogistics.task.web.beans.EmployeeBeen;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Abhishek Raj on 24/6/17.
 */

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeServiceImpl employeeService;

    @Autowired
    private EmployeeRepo employeeRepo;

    @PostMapping(value = "/employee")
    public Employee saveEmployee(@RequestBody Employee employee)throws AppException{
        employee.setJoiningDate(new Date());
        return employeeService.saveEmployee(employee);
    }

    @GetMapping(value = "/employee")
    public List<EmployeeBeen> getAllEmployee()throws AppException{
        List<Employee> allEmployee = employeeService.getAllEmployee();
        return convert(allEmployee);

    }

    @GetMapping(value = "/employee/{empId}")
    public EmployeeBeen getEmployeeById(@PathVariable Long empId)throws AppException{
        Employee employee = employeeService.getEmployeeById(empId);
        return convertEmployee(employee);
    }


    @GetMapping(value = "/all/employee")
    public int totalEmployee()throws AppException{
        int empCount = employeeService.totalEmployee();
        return empCount;
    }

    @DeleteMapping(value = "/employee/{empId}")
    public Boolean deleteEmployeeById(@PathVariable Long empId)throws AppException{
        Boolean flag = employeeService.deleteEmployee(empId);
        return flag;
    }


    @PutMapping(value = "/employee/{empId}")
    public Employee updateEmployee(@PathVariable Long empId, @RequestBody Employee employee)throws AppException{
        Employee updateEmp = employeeService.getEmployeeById(empId);
        updateEmp.setDepartment(employee.getDepartment());
        updateEmp.setName(employee.getName());
        updateEmp.setEmailId(employee.getEmailId());
        updateEmp.setGender(employee.getGender());
        updateEmp.setMobileNo(employee.getMobileNo());
        updateEmp.setDesignation(employee.getDesignation());
        updateEmp = employeeService.saveEmployee(updateEmp);
        return updateEmp;
    }


    private List<EmployeeBeen> convert(List<Employee> employees){
        List<EmployeeBeen> list = new ArrayList<>(employees.size());
        for(Employee oneEmployee: employees){
            EmployeeBeen employeeBeen = convertEmployee(oneEmployee);
            list.add(employeeBeen);
        }
        return list;
    }

    private EmployeeBeen convertEmployee(Employee oneEmployee) {
        EmployeeBeen employeeBean = new EmployeeBeen();
        BeanUtils.copyProperties(oneEmployee , employeeBean);
        return employeeBean;
    }
}
