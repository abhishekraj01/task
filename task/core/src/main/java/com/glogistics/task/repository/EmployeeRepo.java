package com.glogistics.task.repository;

import com.glogistics.task.persistence.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Abhishek Raj on 24/6/17.
 */
public interface EmployeeRepo extends JpaRepository<Employee,Long> {
}
