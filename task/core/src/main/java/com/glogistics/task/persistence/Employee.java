package com.glogistics.task.persistence;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Abhishek Raj on 24/6/17.
 */



@Entity
@Table(name = "employee")
@Getter
@Setter
@ToString
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long empId;

    @Column(name = "name")
    private String name;

    @Column(name = "department")
    private String department;

    @Column(name = "designation")
    private String designation;

    @Column(name = "gender")
    private String gender;

    @Column(name = "joining_date")
    private Date joiningDate;

    @Column(unique = true, name = "mobile_no")
    private Long mobileNo;

    @Column(name = "sallery")
    private Double sallery;

    @Email(message="Please provide a valid email address")
    @Column(unique = true , name = "email_id")
    private String emailId;


}
