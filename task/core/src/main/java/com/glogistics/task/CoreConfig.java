package com.glogistics.task;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


/**
 * Created by Abhishek Raj on 24/6/17.
 */


@SpringBootApplication
@ComponentScan
@EnableJpaRepositories
@EntityScan
public class CoreConfig {
}
