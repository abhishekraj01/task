package com.mychefstable.task_code.web.beans;

import lombok.Data;

import java.util.Date;

/**
 * Created by Abhishek Raj on 24/6/17.
 */

@Data
public class EmployeeBeen {
    protected Long empId;
    protected String name;
    protected String department;
    protected String designation;
    protected String gender;
    protected Date joiningDate;
    protected Long mobileNo;
    protected String emailId;
    protected Double sallery;
}
