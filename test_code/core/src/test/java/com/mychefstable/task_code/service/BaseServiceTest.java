package com.mychefstable.task_code.service;


import com.mychefstable.task_code.CoreConfig;
import com.mychefstable.task_code.exception.AppException;
import com.mychefstable.task_code.persistence.Employee;
import com.mychefstable.task_code.repository.EmployeeRepo;
import com.mychefstable.task_code.service.serviceImpl.EmployeeServiceImpl;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 * Created by Abhishek Raj on 24/6/17.
 */


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CoreConfig.class})
@SpringBootTest
@Transactional
public abstract class BaseServiceTest {

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private EmployeeServiceImpl employeeService;

    protected Employee addEmployee(String name, String department, String designation, String gender, Date joiningDate, Long mobileNo, Double sallery, String emailId) throws AppException {
        Employee employee = new Employee();
        employee.setName(name);
        employee.setDepartment(department);
        employee.setDesignation(designation);
        employee.setGender(gender);
        employee.setJoiningDate(joiningDate);
        employee.setMobileNo(mobileNo);
        employee.setSallery(sallery);
        employee.setEmailId(emailId);
        employee = employeeService.saveEmployee(employee);
        return employee;
    }


}
