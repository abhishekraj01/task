package com.mychefstable.task_code.service;

import com.mychefstable.task_code.exception.AppException;
import com.mychefstable.task_code.persistence.Employee;
import com.mychefstable.task_code.repository.EmployeeRepo;
import com.mychefstable.task_code.service.serviceImpl.EmployeeServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * Created by Abhishek Raj on 24/6/17.
 */

public class EmployeeServiceTest extends BaseServiceTest{

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private EmployeeServiceImpl employeeService;

    @Before
    public void init(){
        employeeRepo.deleteAll();
    }

    @Test
    public void saveEmployeeTest()throws AppException {

        Employee employeeOne = addEmployee("Abhishek Raj","Software Development", "Developer", "Male", new Date(),7838993975L,28000.00,"abhishekkumarraj03@gmail.com");
        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        Employee employeeTwo = addEmployee("Arun Kumar", "Software Development", "Trainee", "Male", new Date(), 9410637053L,15000.9,"arun@gmail.com");
        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

    }


    @Test
    public void getAllEmployeeTest()throws AppException{
        Employee employeeOne = addEmployee("Abhishek Raj","Software Development", "Developer", "Male", new Date(),7838993975L,28000.00,"abhishekkumarraj03@gmail.com");
        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        Employee employeeTwo = addEmployee("Arun Kumar", "Software Development", "Trainee", "Male", new Date(), 9410637053L,15000.9,"arun@gmail.com");
        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        List<Employee> employeeInDb = employeeService.getAllEmployee();
        Assert.assertEquals(employees,employeeInDb);

    }

    @Test
    public void getEmployeeByIdTest()throws AppException{
        Employee employeeOne = addEmployee("Abhishek Raj","Software Development", "Developer", "Male", new Date(),7838993975L,28000.00,"abhishekkumarraj03@gmail.com");
        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        Employee employeeTwo = addEmployee("Arun Kumar", "Software Development", "Trainee", "Male", new Date(), 9410637053L,15000.9,"arun@gmail.com");
        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        Employee oneEmployee = employeeService.getEmployeeById(employeeOne.getEmpId());

        Employee employee = employeeRepo.findOne(employeeOne.getEmpId());
        Assert.assertEquals(oneEmployee,employeeOne);

    }

    @Test
    public void totalEmployeeTest()throws AppException{
        Employee employeeOne = addEmployee("Abhishek Raj","Software Development", "Developer", "Male", new Date(),7838993975L,28000.00,"abhishekkumarraj03@gmail.com");
        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        Employee employeeTwo = addEmployee("Arun Kumar", "Software Development", "Trainee", "Male", new Date(), 9410637053L,15000.9,"arun@gmail.com");
        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        int totalEmp = employeeService.totalEmployee();
        Assert.assertEquals(totalEmp, employees.size());
    }

    @Test
    public void deleteEmployeeTest()throws AppException{
        Employee employeeOne = addEmployee("Abhishek Raj","Software Development", "Developer", "Male", new Date(),7838993975L,28000.00,"abhishekkumarraj03@gmail.com");
        List<Employee> employees = employeeRepo.findAll();
        Assert.assertEquals(1,employees.size());

        Employee employeeTwo = addEmployee("Arun Kumar", "Software Development", "Trainee", "Male", new Date(), 9410637053L,15000.9,"arun@gmail.com");
        employees = employeeRepo.findAll();
        Assert.assertEquals(2,employees.size());

        Boolean flag = employeeService.deleteEmployee(employeeOne.getEmpId());

        employees = employeeService.getAllEmployee();
        Assert.assertEquals(1,employees.size());

        Assert.assertEquals(flag,Boolean.TRUE);

    }

    @After
    public void destroy(){
        employeeRepo.deleteAll();
    }
}
