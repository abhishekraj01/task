package com.mychefstable.task_code.service;

import com.mychefstable.task_code.exception.AppException;
import com.mychefstable.task_code.persistence.Employee;

import java.util.List;

/**
 * Created by Abhishek Raj on 24/6/17.
 */

public interface EmployeeService {

    public Employee saveEmployee(Employee employee) throws AppException;

    public List<Employee> getAllEmployee() throws AppException;

    public int totalEmployee() throws AppException;

    public Employee getEmployeeById(Long empId) throws AppException;

    public Boolean deleteEmployee(Long empId) throws AppException;


}
