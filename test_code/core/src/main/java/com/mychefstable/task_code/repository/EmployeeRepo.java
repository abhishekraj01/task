package com.mychefstable.task_code.repository;

import com.mychefstable.task_code.persistence.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Abhishek Raj on 24/6/17.
 */
public interface EmployeeRepo extends JpaRepository<Employee,Long> {
}
