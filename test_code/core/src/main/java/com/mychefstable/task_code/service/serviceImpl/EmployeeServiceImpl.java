package com.mychefstable.task_code.service.serviceImpl;

import com.mychefstable.task_code.exception.AppException;
import com.mychefstable.task_code.persistence.Employee;
import com.mychefstable.task_code.repository.EmployeeRepo;
import com.mychefstable.task_code.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.springframework.util.Assert.isNull;
import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;
import java.util.List;

/**
 * Created by Abhishek Raj on 24/6/17.
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;


    @Override
    public Employee saveEmployee(Employee employee) throws AppException {
        notNull(employee.getName(),"Name Can Not Be Null");
        notNull(employee.getDepartment(),"Department Name Can Not Be Null");
        notNull(employee.getDesignation(),"Designation Can Not Be Null");

        return employeeRepo.save(employee);
    }

    @Override
    public List<Employee> getAllEmployee() throws AppException {
        List<Employee> employees =  employeeRepo.findAll();
        return employees;
    }

    @Override
    public int totalEmployee() throws AppException {
        List<Employee> employees = employeeRepo.findAll();
        int totalEmp = employees.size();
        return totalEmp;
    }

    @Override
    public Employee getEmployeeById(Long empId) throws AppException {
        Employee employee = employeeRepo.findOne(empId);
        return employee;
    }

    @Override
    public Boolean deleteEmployee(Long empId) throws AppException {
        Employee employee = employeeRepo.findOne(empId);
        notNull(employee,"Employee Not Exist In DataBase");
          if (employee != null){
              employeeRepo.delete(empId);
              return Boolean.TRUE;
          }

        return Boolean.FALSE;

    }


}
